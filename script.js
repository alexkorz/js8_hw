

// TASK 1

let string1 = "travel";
let string2 = "hello";
let string3 = "eat";
let string4 = "ski";
let string5 = "lift";

function createArrayFromStrings (...args) {
  let longStrings = 0;
  for (const arg of args) {
    if (arg.length > 3) {
      longStrings++;
    }
  }
  return longStrings;
}

console.log(createArrayFromStrings(string1, string2, string3, string4, string5));

// TASK 2

let people = [
  {
    name: "Іван",
    age: 25,
    sex: "чоловіча"
  },
  {
    name: "Миколай",
    age: 32,
    sex: "чоловіча"
  },
  {
    name: "Настя",
    age: 19,
    sex: "жіноча"
  },
  {
    name: "Оля",
    age: 23,
    sex: "жіноча"
  }
];

let menArray = people.filter(person => person.sex === "чоловіча");
console.log(menArray);

// TASK 3
function filterBy(array, dataType) {
  return array.filter(item => typeof item !== dataType)
}

let testArray = ['hello', 'world', 23, '23', null];

console.log(filterBy(testArray, 'string'));

